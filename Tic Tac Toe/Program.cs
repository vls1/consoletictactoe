﻿using System;

namespace Tic_Tac_Toe
{
    class Program
    {
        static string[,] gameField = new string[3, 3];

        static void Main(string[] args)
        {
            InitField();

            SaveGame();
            LoadGame();

            while (true)
            {
                DrawField();
                PlayerMove();
                DrawField();
            }
        }

        static void InitField()
        {
            for (int i = 0; i < gameField.GetLength(0); i++)
            {
                for (int j = 0; j < gameField.GetLength(1); j++)
                {
                    gameField[i, j] = "*";
                }
            }
        }
        static void DrawField()
        {
            for (int i = 0; i < gameField.GetLength(0); i++)
            {
                Console.WriteLine();
                for (int j = 0; j < gameField.GetLength(1); j++)
                {
                    Console.Write(gameField[i, j] + " ");
                }
            }
        }
        static void PlayerMove() 
        {
            int x = 0;
            int y = 0;
            bool isEmpty = false;

            Console.WriteLine("Ход игрока.");

            x = PlayerChoise();
            y = PlayerChoise();

            while (!isEmpty)
            {
                if (!gameField[x, y].Equals("*"))
                {
                    x = PlayerChoise();
                    y = PlayerChoise();
                }
                else 
                {
                    gameField[x, y] = "x";
                    Console.WriteLine(gameField[x, y]);
                    isEmpty = true;
                }
            }
        }
        static int PlayerChoise() 
        {
            Console.WriteLine("Введите X: ");
            return Convert.ToInt32(Console.ReadLine());
        }
        static void ComputerMove() 
        {
            int x = 0;
            int y = 0;

            bool isEmpty = false;

            Random rnd = new Random();
            x = rnd.Next(0, 3); 
            y = rnd.Next(0, 3);

            while (!isEmpty)
            {
                if (!gameField[x, y].Equals("*"))
                {
                    x = rnd.Next(0, 3);
                    y = rnd.Next(0, 3);
                }
                else
                {
                    gameField[x, y] = "0";
                    Console.WriteLine(gameField[x, y]);
                    isEmpty = true;
                }
            }
        }
        static void SaveGame()
        {
            string filePath = @"C:\gameData.txt";
            string text = "Super text 3000";

            File.WriteAllText(filePath, text);

            Console.WriteLine("Saved...");
        }
        static void LoadGame()
        {
            string gameData = File.ReadAllText(@"C:\gameData.txt");

            //Split разбивает строку на моссив строк по разделителю
            string[] parsedGameData = gameData.Split(' ');

            for (int i = 0; i < parsedGameData.Length; i++)
            {
                Console.WriteLine(parsedGameData[i]);
            }

            Console.WriteLine("Loaded...");
        }
    }
}
